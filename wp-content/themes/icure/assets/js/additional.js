"use strict";

jQuery(($) => {
  const loadMoreNews = () => {
    const $container = $('.blogs__list');

    if ($container.length === 0) {
      return;
    }

    const template = wp.template('blog-template');
    const $loader = $('.loader');

    $('.blogs__more .btn').on('click',  (e) => {
      e.preventDefault();

      const $btn = $(e.currentTarget);
      const current_page = +$btn.attr('data-page') - 1;
      const per_page = 4;
      const offset_start = 3;
      const offset = current_page * per_page + offset_start;

      $loader.addClass('-visible');

      $.get({
        url: wp.endpoint + '/posts?_embed&per_page=' + per_page + '&offset=' + offset,
        cache: true,
        success: (data, textStatus, request) => {
          for (const post of data) {
            $container.append(template({
              link: post.link,
              title: post.title.rendered,
              excerpt: post.excerpt.rendered,
              thumbnail: post.thumbnail,
              formattedDate: post.formattedDate,
            }));
          }

          $loader.removeClass('-visible');
          $btn.attr('data-page', +$btn.attr('data-page') + 1);

          if (offset + per_page >= request.getResponseHeader('X-WP-Total')) {
            $btn.remove();
          }
        }
      });
    })
  }

  const init = () => {
    loadMoreNews();
  }

  init();
});
