"use strict";

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

jQuery(function ($) {
  var loadMoreNews = function loadMoreNews() {
    var $container = $('.blogs__list');

    if ($container.length === 0) {
      return;
    }

    var template = wp.template('blog-template');
    var $loader = $('.loader');
    $('.blogs__more .btn').on('click', function (e) {
      e.preventDefault();
      var $btn = $(e.currentTarget);
      var current_page = +$btn.attr('data-page') - 1;
      var per_page = 4;
      var offset_start = 3;
      var offset = current_page * per_page + offset_start;
      $loader.addClass('-visible');
      $.get({
        url: wp.endpoint + '/posts?_embed&per_page=' + per_page + '&offset=' + offset,
        success: function success(data, textStatus, request) {
          var _iterator = _createForOfIteratorHelper(data),
            _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var post = _step.value;
              $container.append(template({
                link: post.link,
                title: post.title.rendered,
                excerpt: post.excerpt.rendered,
                thumbnail: post.thumbnail,
                formattedDate: post.formattedDate
              }));
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          $loader.removeClass('-visible');
          $btn.attr('data-page', +$btn.attr('data-page') + 1);

          if (offset + per_page >= request.getResponseHeader('X-WP-Total')) {
            $btn.remove();
          }
        }
      });
    });
  };

  var init = function init() {
    loadMoreNews();
  };

  init();
});
