<?php if (is_page() || is_home()) : ?>
  <article class="homepage__form">
  <div class="wrapper">
    <div class="homepage__form_details">
      <h2>Ready for more?</h2>
      <p>
        <b>Just checking in?</b>
        <span> Stop by our <a href="https://www.linkedin.com/company/icure-ehealth" target="_blank">
                  <?php echo get_attachment_image(215, 'full', 'Linkedin', 'Linkedin'); ?>
                </a> or <a href="https://twitter.com/icure_ehealth" target="_blank">
                  <?php echo get_attachment_image(216, 'full', 'Twitter', 'Twitter'); ?>
                </a> to say hello =) </span>
      </p>
    </div>
    <div class="homepage__form_body">
      <?php echo get_attachment_image(217, 'full', null, null, 'form--bg'); ?>
      <div class="form">
        <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
      </div>
    </div>
  </div>
</article>
<?php endif; ?>

<?php
$heading = get_field('heading', 'options');
$footer_menu = get_field('footer_menu', 'options');
$bottom_links = get_field('bottom_links', 'options');
?>

<footer>
  <div class="wrapper">
    <div class="footer__title"><?php echo esc_html($heading); ?></div>

    <?php if (!empty($footer_menu)) : ?>
      <ul class="footer__list">
        <?php foreach ($footer_menu as $column) :
          $items = $column['items'];
          $column_title_attr = !empty($column['title']) ? 'data-title="' . $column['title'] . '"' : null; 
          ?>
          <li <?php echo $column_title_attr; ?>>
            <?php foreach ($items as $item) : ?>
              <?php
              $item_classes = [];
              $item_target_attr = null;
              $item_attributes = $item['link']['link_attributes'];

              if (!empty($item['link']['link_css_class'])) {
                $item_classes[] = $item['link']['link_css_class'];
              }

              if (!empty($item['link']['link']['target'])) {
                $item_classes[] = 'external';
                $item_target_attr = 'target=' . $item['link']['link']['target'] . '"';
              }
              ?>
              <a class="<?php echo esc_attr(implode(' ', $item_classes)); ?>" href="<?php echo esc_url($item['link']['link']['url']); ?>" <?php echo $item_target_attr; ?> <?php echo $item_attributes; ?>><?php echo esc_html($item['link']['link']['title']); ?></a>
            <?php endforeach; ?>
          </li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>

    <?php if (!empty($bottom_links)) : ?>
      <div class="footer__terms">
        <?php foreach ($bottom_links as $bottom_link) : ?>
          <?php get_field_link($bottom_link['link']); ?>
        <?php endforeach; ?>

        <span>© iCure <?php the_time('Y'); ?></span>
      </div>
    <?php endif; ?>
  </div>
</footer>
