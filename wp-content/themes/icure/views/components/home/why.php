<?php
$image = get_sub_field('image');
$title = get_sub_field('title');
$text = get_sub_field('text');
?>

<?php if (!empty($title) || !empty($text)) : ?>
  <article class="homepage__why">
    <section class="homepage__why_image">
      <?php echo get_attachment_image($image, 'large', strip_tags($title), strip_tags($title)); ?>
    </section>
    <section class="homepage__why_details">
      <?php if (!empty($title)) : ?>
        <h2><?php echo wp_kses_post($title); ?></h2>
      <?php endif; ?>

      <?php echo wp_kses_post($text); ?>
    </section>
  </article>
<?php endif; ?>
