<?php
$clients_title = get_sub_field('title');
$items = get_sub_field('items');
?>

<?php if (!empty($items)) : ?>
  <article class="homepage__clients">
    <?php if (!empty($clients_title)) : ?>
      <h2><?php echo esc_html($clients_title); ?></h2>
    <?php endif; ?>

    <div class="wrapper">
      <?php foreach ($items as $item) : ?>
        <?php if (!empty($item['link']['url'])) : ?>
          <a href="<?php echo esc_url($item['link']['url']); ?>" target="_blank" rel="noopener noreferrer">
            <?php echo get_attachment_image($item['logo'], 'medium'); ?>
          </a>
        <?php else : ?>
          <?php echo get_attachment_image($item['logo'], 'medium'); ?>
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
  </article>
<?php endif; ?>
