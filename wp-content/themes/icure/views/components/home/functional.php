<?php
$functional_title = get_sub_field('title');
$items = get_sub_field('items');
?>

<?php if (!empty($functional_title) || !empty($items)) : ?>
  <article class="homepage__functional">
    <?php if (!empty($functional_title)) : ?>
      <div class="homepage__functional_title">
        <div class="wrapper">
          <h2><?php echo esc_html($functional_title); ?></h2>
        </div>
      </div>
    <?php endif; ?>

    <?php if (!empty($items)) : ?>
      <div class="homepage__functional_list">
        <div class="wrapper">
          <ul>
            <?php foreach ($items as $item) :
              $item_title = $item['title'];
              ?>
              <li>
                <?php echo get_attachment_image($item['icon'], 'thumbnail', strip_tags($item_title), strip_tags($item_title)); ?>

                <?php if (!empty($item_title)) : ?>
                  <h3><?php echo esc_html($item_title); ?></h3>
                <?php endif; ?>

                <?php echo wp_kses_post($item['text']); ?>

                <?php get_field_link($item['link'], 'more', 'More'); ?>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    <?php endif; ?>
  </article>
<?php endif; ?>
