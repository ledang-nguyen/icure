<?php
$heading_title = get_sub_field('heading_title');
$heading_text = get_sub_field('heading_text');
$heading_items = get_sub_field('heading_items');

$details_title = get_sub_field('details_title');
$details_text = get_sub_field('details_text');
$details_image = get_sub_field('details_image');
$details_items = get_sub_field('details_items');
?>


<?php if (!empty($heading_items) || !empty($details_items)) : ?>
  <article class="homepage__about">
    <?php if (!empty($heading_items)) : ?>
      <div class="homepage__about_title">
        <div class="wrapper">
          <div class="title--text">
            <?php if (!empty($heading_title)) :  ?>
              <h2><?php echo esc_html($heading_title); ?></h2>
            <?php endif; ?>

            <?php echo wp_kses_post($heading_text); ?>
          </div>

          <ul class="title--details">
            <?php foreach ($heading_items as $heading_item) : ?>
              <li>
                <b><?php echo esc_html($heading_item['title']); ?></b>
                <span><?php echo esc_html($heading_item['caption']); ?></span>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    <?php endif; ?>

    <div class="homepage__about_details">
      <div class="wrapper">
        <?php if (!empty($details_title) || !empty($details_text)) : ?>
          <div class="details--title">
            <?php if (!empty($details_title)) : ?>
              <h3><?php echo esc_html($details_title) ?></h3>
            <?php endif; ?>

            <?php echo wp_kses_post($details_text); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($details_items)) : ?>
          <div class="details--wrapper">
            <?php if (!empty($details_image)) : ?>
              <div class="details--image">
                <?php echo get_attachment_image($details_image, 'large', strip_tags($details_title), strip_tags($details_title)); ?>
              </div>
            <?php endif; ?>

            <ul class="details-texts">
              <?php foreach ($details_items as $details_item) : ?>
                <li>
                  <?php if (!empty($details_item['title'])) : ?>
                    <h4><?php echo wp_kses_post($details_item['title']); ?></h4>
                  <?php endif; ?>

                  <?php echo wp_kses_post($details_item['text']); ?>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </article>
<?php endif; ?>
