<?php
  $image = get_sub_field('image');
  $hero_title = get_sub_field('title');
  $text = get_sub_field('text');
  $button = get_sub_field('button');
  $link = get_sub_field('link');
?>

<?php if (!empty($hero_title)) : ?>
  <article class="homepage__hero">
    <div class="homepage__hero_solutions">ehealth solutions</div>
    <div class="wrapper">
      <section class="homepage__hero_logo">
        <?php echo get_attachment_image($image, 'medium', $hero_title, $hero_title); ?>
      </section>
      <section class="homepage__hero_details">
        <h1><?php echo esc_html($hero_title); ?></h1>

        <?php echo wp_kses_post($text); ?>

        <?php get_field_link($button, 'btn btn-fill'); ?>
      </section>

      <?php get_field_link($link, 'homepage__hero_link'); ?>
    </div>
  </article>
<?php endif; ?>
