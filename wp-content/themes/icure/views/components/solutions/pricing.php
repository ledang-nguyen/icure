<?php
$items = get_sub_field('items');
$anchor = get_sub_field('anchor');
?>

<?php if (!empty($items)) : ?>
  <article class="solutions__prices">
    <?php if (!empty($anchor)) : ?>
      <div class="anchor" id="<?php echo esc_attr($anchor); ?>"></div>
    <?php endif; ?>
    <h2>Pricing</h2>
    <div class="wrapper">
      <?php foreach ($items as $item) : ?>
        <section class="solutions__prices_item">
          <div class="item--hrader <?php echo $item['free'] ? 'free' : null; ?>">
            <div class="item--hrader-title"><?php echo esc_html($item['title']); ?></div>
            <div class="item--hrader-description"><?php echo wp_kses_post($item['description']); ?></div>
          </div>
          <div class="item--body">
            <?php if (!empty($item['includes'])) : ?>
              <div class="incudes">
                <div class="item-title-grey">Includes:</div>

                <?php echo wp_kses_post($item['includes']); ?>
              </div>
            <?php endif; ?>

            <?php if (!empty($item['optional'])) : ?>
              <div class="options">
                <div class="item-title-grey">Optional:</div>

                <?php echo wp_kses_post($item['optional']); ?>
              </div>
            <?php endif; ?>

            <?php if (!empty($item['caption'])) : ?>
              <div class="item-title-grey"><?php echo esc_html($item['caption']); ?></div>
            <?php endif; ?>

            <?php if (!empty($item['link']['url'])) : ?>
              <div class="item--link">
                <?php get_field_link($item['link'], 'btn btn-stroke', 'Get In Touch'); ?>
              </div>
            <?php endif; ?>
          </div>
        </section>
      <?php endforeach; ?>
    </div>
  </article>
<?php endif; ?>
