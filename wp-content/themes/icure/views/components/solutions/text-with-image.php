<?php
$twm_title = get_sub_field('title');
$text = get_sub_field('text');
$image = get_sub_field('image');
$image_mobile = get_sub_field('image_mobile');
$anchor = get_sub_field('anchor');
?>

<?php if (!empty($image)) : ?>
  <article class="solutions__model">
    <?php if (!empty($anchor)) : ?>
      <div class="anchor" id="<?php echo esc_attr($anchor); ?>"></div>
    <?php endif; ?>

    <div class="wrapper">
      <h2><?php echo wp_kses_post($twm_title); ?></h2>

      <?php echo wp_kses_post($text); ?>

      <?php echo get_attachment_image($image, 'full', strip_tags($twm_title), strip_tags($twm_title), 'solutions__model-image'); ?>
      <?php echo get_attachment_image($image_mobile, 'full', strip_tags($twm_title), strip_tags($twm_title), 'solutions__model-image--mobile'); ?>
    </div>
  </article>
<?php endif; ?>
