<?php
$items = get_sub_field('items');
?>

<?php if (!empty($items)) : ?>
  <article class="solutions__list">
    <div class="wrapper">
      <ul>
        <?php foreach ($items as $item) : ?>
          <li class="solutions__list_item">
            <?php echo get_attachment_image($item['image'], 'medium', $item['title'], $item['title']); ?>
            <span><?php echo esc_html($item['title']); ?></span>
            <?php get_field_link($item['link'], '', 'More'); ?>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </article>
<?php endif; ?>
