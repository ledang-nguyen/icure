<?php
$hero_title = get_sub_field('title');
$content = get_sub_field('content');
?>

<?php if (!empty($hero_title)) : ?>
  <article class="solutions__hero">
    <div class="wrapper">
      <h1><?php echo esc_html($hero_title); ?></h1>

      <?php foreach ($content as $item) : ?>
        <div class="solutions__hero_<?php echo esc_attr($item['type']); ?>">
          <?php echo wp_kses_post($item['text']); ?>
        </div>
      <?php endforeach; ?>
    </div>
  </article>
<?php endif; ?>
