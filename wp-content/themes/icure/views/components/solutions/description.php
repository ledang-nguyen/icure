<?php
$items = get_sub_field('items');
?>

<?php if (!empty($items)): ?>
  <article class="solutions__description">
    <?php foreach ($items as $item) : ?>
      <section class="solutions__description_item <?php echo esc_attr($item['class']); ?>">
        <div class="anchor" id="<?php echo esc_attr($item['anchor']); ?>"></div>

        <?php if ($item['full_width_image']) : ?>
          <?php echo get_attachment_image($item['image'], 'full', $item['title'], $item['title'], 'item--image'); ?>
          <?php echo get_attachment_image($item['image_mobile'], 'full', $item['title'], $item['title'], 'item--image-mobile'); ?>
        <?php endif; ?>

        <div class="wrapper">
          <?php if (!$item['full_width_image']) : ?>
            <?php echo get_attachment_image($item['image'], 'full', $item['title'], $item['title'], 'item--image'); ?>
            <?php echo get_attachment_image($item['image_mobile'], 'full', $item['title'], $item['title'], 'item--image-mobile'); ?>
          <?php endif; ?>

          <div class="item--details">
            <h3><?php echo esc_html($item['title']); ?></h3>

            <?php echo wp_kses_post($item['text']); ?>

            <?php get_field_link($item['link'], '', 'More'); ?>
          </div>
        </div>
      </section>
    <?php endforeach; ?>
  </article>
<?php endif; ?>
