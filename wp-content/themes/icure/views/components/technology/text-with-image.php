<?php
$image = get_sub_field('image');
$title = get_sub_field('title');
$text = get_sub_field('text');
?>

<?php if (!empty($image)) : ?>
  <article class="technology__developed">
    <?php echo get_attachment_image($image, 'full', $title, $title); ?>
    
    <div class="wrapper">
      <?php if (!empty($title)) : ?>
        <h2><?php echo esc_html($title); ?></h2>
      <?php endif; ?>
      
      <?php echo wp_kses_post($text); ?>
    </div>
  </article>
<?php endif; ?>
