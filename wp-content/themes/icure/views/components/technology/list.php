<?php
$list = get_sub_field('list');
?>

<?php if (!empty($list)) : ?>
  <article class="technology__list">
    <?php foreach ($list as $item) : ?>
      <section class="technology__item">
        <?php if (!empty($item['anchor'])) : ?>
          <div class="anchor" id="<?php echo esc_attr($item['anchor']); ?>"></div>
        <?php endif; ?>
        
        <div class="technology__item_image">
          <?php echo get_attachment_image($item['image'], 'large', $item['title'], $item['title']); ?>
        </div>
        <div class="technology__item_details">
          <h2><?php echo esc_html($item['title']); ?></h2>
  
          <?php echo wp_kses_post($item['text']); ?>
  
          <?php get_field_link($item['link'], '', 'More'); ?>
        </div>
      </section>
    <?php endforeach; ?>
  </article>
<?php endif; ?>
