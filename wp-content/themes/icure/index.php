<?php
/**
 * The main template file
 */
?>

<?php get_header(); ?>

<main>
  <h1>Blog</h1>
  <article class="blogs__list">
    <?php while (have_posts()) : the_post(); ?>
      <section class="blog">
        <a href="<?php the_permalink(); ?>" class="blog--image">
          <?php the_post_thumbnail('large'); ?>
        </a>
        <div class="blog--details">
          <time><?php the_time('F j, Y'); ?></time>
          <h2>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h2>
          <?php the_excerpt(); ?>
          <a href="<?php the_permalink(); ?>">Read more</a>
        </div>
      </section>
    <?php endwhile; ?>
  </article>
  <div class="blogs__more">
    <a href="" class="btn btn-stroke" data-page="1">More</a>
  </div>
  <div class="loader">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
  </div>
</main>

<script type="text/html" id="tmpl-blog-template">
  <section class="blog">
    <a href="{{data.link}}" class="blog--image">
      {{{data.thumbnail}}}
    </a>
    <div class="blog--details">
      <time>{{data.formattedDate}}</time>
      <h2>
        <a href="{{data.link}}">{{data.title}}</a>
      </h2>
      {{{data.excerpt}}}
      <a href="{{data.link}}">Read more</a>
    </div>
  </section>
</script>

<?php get_footer(); ?>
