<?php
/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
  wp_enqueue_style('main', getCssPath() . 'app.css', null, getFileTime('css/app.css'), 'all');
  wp_enqueue_style('additional', getCssPath() . 'additional.css', null, getFileTime('css/additional.css'), 'all');
  wp_enqueue_script('main', getJsPath() . 'app.js', null, getFileTime('js/app.js'), true);

  // Check if IE
  $http = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
  if (preg_match('~MSIE|Internet Explorer~i', $http) || (strpos($http, 'Trident/7.0') !== false && strpos($http, 'rv:11.0') !== false)) {
    wp_enqueue_script('additional-ie', getJsPath() . 'additional-ie.js', ['jquery'], getFileTime('js/additional-ie.js'), true);
  } else {
    wp_enqueue_script('additional', getJsPath() . 'additional.js', ['jquery'], getFileTime('js/additional.js'), true);
  }

  if (is_home()) {
    wp_enqueue_script('wp-util');
  }

  wp_dequeue_style('wp-block-library');
  wp_dequeue_style('wp-block-library-theme');
  wp_dequeue_style('wc-block-style');

  wp_localize_script('main', 'wp', [
    'endpoint' => esc_url_raw(rest_url('wp/v2')),
  ]);
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
  /**
   * Enable plugins to manage the document title
   * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
   */
  add_theme_support('title-tag');

  /**
   * Register navigation menus
   * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
   */
  register_nav_menus([
    'main-nav' => __('Main Navigation'),
  ]);

  /**
   * Enable post thumbnails
   * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
   */
  add_theme_support('post-thumbnails');

  /**
   * Enable HTML5 markup support
   * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
   */
  add_theme_support('html5');

  /**
   * Add custom logo support
   * @link https://developer.wordpress.org/themes/functionality/custom-logo/
   */
  add_theme_support('custom-logo', [
    'flex-width'  => true,
    'flex-height' => true,
    'uploads'     => true,
    'header-text' => false,
  ]);
});

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
  $config = [
    'before_widget' => '<aside class="widget %1$s %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
    'before_content' => '<div class="widget-content">',
    'after_content' => '</div>',
  ];

  register_sidebar([
    'name' => __('Sidebar'),
    'id' => 'sidebar'
  ] + $config);
});

/**
 * Add custom logo if exists
 */
add_action('login_enqueue_scripts', function () {
  $logo_id = get_theme_mod('custom_logo');
  $logo_url = wp_get_attachment_image_url($logo_id, 'full');

  if (empty($logo_id)) return;

  $styles = "<style>
    .login #login h1 a {
      background-image: none, url(${logo_url});
      background-size: contain;
      width: auto;
      max-height: 400px;
    }
  </style>";

  echo wp_kses($styles, ['style' => true]);
});

/**
 * Change logo url
 */
add_filter('login_headerurl', function () {
  $site_url = home_url();

  return ($site_url);
});

/**
 * Extend WordPress search to include custom fields
 *
 * Join posts and postmeta tables
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
add_filter('posts_join', function ($join) {
  global $wpdb;

  if (is_search()) {
    $join .=' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
  }

  return $join;
});

/**
 * Modify the search query with posts_where
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
add_filter('posts_where', function ($where) {
  global $wpdb;

  if (is_search()) {
    $where = preg_replace('/\(\s*' . $wpdb->posts . '.post_title\s+LIKE\s*(\'[^\']+\')\s*\)/', '(' . $wpdb->posts . '.post_title LIKE $1) OR (' . $wpdb->postmeta . '.meta_value LIKE $1)', $where);
  }

  return $where;
});

/**
 * Prevent duplicates
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
add_filter('posts_distinct', function ($where) {
  global $wpdb;

  if (is_search()) {
    return 'DISTINCT';
  }

  return $where;
});

/**
 * Change Yoast SEO Metabox order
 */
add_filter('wpseo_metabox_prio', function () {
  return 'low';
});

/**
 * Add shortcode for current year
 */
add_shortcode('current_year', function () {
  return date('Y');
});
