<?php
/**
 * Custom fields for REST API
 */
add_action('rest_api_init', function () {
  register_rest_field('post', 'formattedDate', array(
      'get_callback' => function ($post, $field_name, $request) {
        return get_the_time('F j, Y', $post['id']);
      },
      'update_callback' => null,
      'schema' => null
  ));

  register_rest_field('post', 'thumbnail', array(
      'get_callback' => function ($post, $field_name, $request) {
        if (get_the_post_thumbnail($post['id'])) {
          return get_the_post_thumbnail($post['id'], 'large');
        }

        return null;
      },
      'update_callback' => null,
      'schema' => null
  ));
});

/**
 * Remove user list endpoint from rest api
 */
add_filter('rest_endpoints', function($aryEndpoints){
  if(isset($aryEndpoints['/wp/v2/users'])){
    unset($aryEndpoints['/wp/v2/users']);
  }
  if(isset($aryEndpoints['/wp/v2/users/(?P<id>[\d]+)'])){
    unset($aryEndpoints['/wp/v2/users/(?P<id>[\d]+)']);
  }

  return $aryEndpoints;
});
