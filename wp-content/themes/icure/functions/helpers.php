<?php
/**
 * Returns an instance of the BemMenu class with the following arguments
 */
if (!function_exists('get_bem_menu')) {
  function get_bem_menu($location = 'main_menu', $css_class_prefix = 'mainMenu', $css_class_modifiers = null, $depth = 2) {
    if (!has_nav_menu($location)) return;

    // Check to see if any css modifiers were supplied
    if ($css_class_modifiers) {
      if (is_array($css_class_modifiers)) {
        $modifiers = implode(' ', $css_class_modifiers);
      } elseif (is_string($css_class_modifiers)) {
        $modifiers = ' ' . $css_class_modifiers;
      }
    } else {
      $modifiers = '';
    }

    $args = [
      'theme_location'    => $location,
      'container'         => false,
      'items_wrap'        => '<ul class="' . $css_class_prefix . $modifiers . '">%3$s</ul>',
      'walker'            => new BemMenu($css_class_prefix, true),
      'link_before'       => '<span>',
      'link_after'        => '</span>',
      'depth'             => $depth,
    ];

    return wp_nav_menu($args);
  };
}

/**
 * Includes a widget template to widget class
 */
if (!function_exists('get_widget_template')) {
  function get_widget_template($widget, $args, $instance) {
    $template_name = sanitize_title($widget->name);
    $template = locate_template("views/widgets/{$template_name}.php");
    $title = $instance['title'];

    echo wp_kses_post($args['before_widget']);
    echo (!empty($title) ? wp_kses_post($args['before_title'] . esc_attr($title) . wp_kses_post($args['after_title'])) : null);

    if ($template) {
      echo ($args['before_content'] ?: null);
      include $template;
      echo ($args['after_content'] ?: null);
    }

    echo wp_kses_post($args['after_widget']);
  }
}

/**
 * Render the link from ACF Link field
 */
if (!function_exists('get_field_link')) {
  function get_field_link($link, $class = '', $default = 'Learn More') {
    if (empty($link) && !is_array($link)) return false;

    $link_title = !empty($link['title']) ? $link['title'] : $default;

    $output = "<a ";
    $output .= !empty($class) ? "class='{$class}'" : null;
    $output .= "href='{$link['url']}'";
    $output .= !empty($link['target']) ? " target='_blank' rel='noopener noreferrer'" : null;
    $output .= !empty($link['download']) ? " download" : null;
    $output .= "><span>{$link_title}</span></a>";

    echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
  }
}

/**
 * Get flexible loop
 */
if (!function_exists('get_flexible_loop')) {
  function get_flexible_loop() {
    while (have_rows('flexible')) {
      the_row();
      $section_name = str_replace('_', '-', get_row_layout());

      echo "<section class=\"Flexible -{$section_name}\">"; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
      get_template_part("views/flexible/{$section_name}");
      echo '</section>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
    }
  }
}

/**
 * Prepare vertical shift for background image
 */
if (!function_exists('get_background_vertical_alignment')) {
  function get_background_vertical_alignment(&$image) {
    if (isset($image['align']) && isset($image['shift']) && $image['align'] === 'custom') {
      $align = $image['shift'] . '%';
    } else {
      $align = $image['align'];
    }

    $result = $align ? "background-position-y: {$align};" : null;

    return $result;
  }
}

/**
 * Get formatted excerpt
 */
if (!function_exists('get_trimmed_excerpt')) {
  function get_trimmed_excerpt(WP_Post $post, $limit = 20) {
    return has_excerpt() ? wp_trim_words($post->post_excerpt, $limit) : wp_trim_words($post->post_content, $limit);
  }
}

/**
 * Get template component with passing the props
 */
if (!function_exists('get_template_component')) {
  function get_template_component($component_name = '', $component_props = [], $template_path = 'views/components') {
    return get_template_part("{$template_path}/{$component_name}", null, $component_props);
  }
}

/**
 * Get actual title
 */
if (!function_exists('get_actual_title')) {
  function get_actual_title() {
      if (is_home()) {
        if ($home = get_option('page_for_posts', true)) {
          return get_the_title($home);
        }

        return __('Latest Posts');
      }

      if (is_archive()) {
        return get_the_archive_title();
      }

      if (is_search()) {
        return sprintf(__('Search Results for: "%s"'), get_search_query());
      }

      if (is_404()) {
        return __('Not Found');
      }

      return get_the_title();
  }
}

/**
 * Has custom sidebar
 */
if (!function_exists('is_custom_sidebar')) {
  function is_custom_sidebar($name = 'sidebar') {
    ob_start();
    dynamic_sidebar($name);

    $sidebar = ob_get_contents();

    ob_end_clean();

    return !empty($sidebar);
  }
}

/**
 * Get timestamp of file
 *
 * @param $fileName
 * @return false|int
 */
function getFileTime($fileName) {
  return filemtime(get_template_directory() . '/' . THEME_ASSETS_PATH . '/' . $fileName);
}


/**
 * Get CSS Directory path
 *
 * @return string
 */
function getCssPath() {
  return get_template_directory_uri() . '/' . THEME_ASSETS_PATH . '/css/';
}

/**
 * Get JS Directory path
 *
 * @return string
 */
function getJsPath() {
  return get_template_directory_uri() . '/' . THEME_ASSETS_PATH . '/js/';
}

/**
 * Get IMG Directory path
 *
 * @return string
 */
function getImgPath() {
  return get_template_directory_uri() . '/' . THEME_ASSETS_PATH . '/img/';
}

/**
 * Get PDF Directory path
 *
 * @return string
 */
function getPdfPath() {
  return get_template_directory_uri() . '/' . THEME_ASSETS_PATH . '/pdf/';
}

/**
 * Add additional class to body
 */
if (!function_exists('additional_body_class')) {
  function additional_body_class() {
    if (is_front_page()) {
      return 'homepage';
    } elseif (is_home() || is_search()) {
      return 'blogs';
    } elseif (get_page_template_slug() === 'templates/solutions.php') {
      return 'solutions';
    } elseif (get_page_template_slug() === 'templates/technology.php') {
      return 'technology';
    }

    return 'article';
  }
}

/**
 * Get attachment image with srcset
 */
if (!function_exists('get_attachment_image')) {
  function get_attachment_image($attachment_id, $size = 'full', $title = null, $alt = null, $class = null) {
    if (empty($attachment_id)) {
      return;
    }

    $attrs = [
        'srcset' => wp_get_attachment_image_srcset($attachment_id, $size),
        'sizes' => wp_get_attachment_image_sizes( $attachment_id, $size),
    ];

    if (!empty($title)) {
      $attrs['title'] = $title;
    }

    if (!empty($alt)) {
      $attrs['alt'] = $alt;
    }

    if (!empty($class)) {
      $attrs['class'] = $class;
    }

    return wp_get_attachment_image($attachment_id, $size, false, $attrs);
  }
}
