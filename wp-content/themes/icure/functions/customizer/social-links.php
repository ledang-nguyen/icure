<?php

/**
 * Register social links customizer fields
 */
if (class_exists('Kirki')) {
  Kirki::add_section('social_links', [
    'title' => esc_html__('Social Links'),
    'priority' => 160,
  ]);

  Kirki::add_field('social_links', [
    'type' => 'repeater',
    'label' => esc_html__('Social Links'),
    'section' => 'social_links',
    'button_label' => esc_html__('Add Social Link'),
    'settings' => 'social_links',
    'row_label' => [
      'type'  => 'field',
      'value' => null,
      'field' => 'network'
    ],
    'default' => [],
    'fields' => [
      'network' => [
        'type' => 'select',
        'label' => esc_html__('Network'),
        'default' => 'facebook',
        'choices' => [
          'facebook' => 'Facebook',
          'twitter' => 'Twitter',
          'linkedin' => 'LinkedIn',
          'instagram' => 'Instagram',
          'youtube' => 'YouTube',
        ],
      ],
      'link' => [
        'type' => 'link',
        'label' => esc_html__('Link'),
      ],
    ],
  ]);
}
