<?php

/**
 * Register contact info customizer fields
 */
if (class_exists('Kirki')) {
  Kirki::add_section('contact_info', [
    'title' => esc_html__('Contact Information'),
    'priority' => 160,
  ]);

  Kirki::add_field('contact_info', [
    'type' => 'textarea',
    'settings' => 'contact_address',
    'label' => esc_html__('Address'),
    'section' => 'contact_info',
    'priority' => 10,
  ]);

  Kirki::add_field('contact_info', [
    'type' => 'text',
    'settings' => 'contact_phone',
    'label' => esc_html__('Phone'),
    'section' => 'contact_info',
    'priority' => 10,
  ]);

  Kirki::add_field('contact_info', [
    'type' => 'email',
    'settings' => 'contact_email',
    'label' => esc_html__('Email'),
    'section' => 'contact_info',
    'priority' => 10,
  ]);
}
