<?php

/**
 * Register services customizer fields
 */
if (class_exists('Kirki')) {
  Kirki::add_section('third_party_services', [
    'title' => esc_html__('Third Party Services'),
    'priority' => 160,
  ]);

  Kirki::add_field('third_party_services', [
    'type' => 'text',
    'settings' => 'google_maps_api_key',
    'label' => esc_html__('Google Maps API Key'),
    'section' => 'third_party_services',
    'priority' => 10,
  ]);
}
