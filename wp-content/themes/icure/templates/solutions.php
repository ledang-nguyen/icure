<?php
/**
 * Template Name: Solutions
 */
?>

<?php get_header(); ?>

<main>
  <?php while (have_rows('components')) :
    the_row();
    $section_name = str_replace('_', '-', get_row_layout()); ?>

    <?php get_template_part("views/components/solutions/{$section_name}"); ?>

  <?php endwhile; ?>
</main>

<?php get_footer(); ?>
