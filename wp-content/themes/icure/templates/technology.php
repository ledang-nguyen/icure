<?php
/**
 * Template Name: Technology
 */
?>

<?php get_header(); ?>

<main>
  <h1><?php the_title(); ?></h1>

  <?php while (have_rows('components')) :
    the_row();
    $section_name = str_replace('_', '-', get_row_layout()); ?>

    <?php get_template_part("views/components/technology/{$section_name}"); ?>

  <?php endwhile; ?>
</main>

<?php get_footer(); ?>
