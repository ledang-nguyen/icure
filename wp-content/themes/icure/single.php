<?php
/**
 * The template for displaying all single posts and attachments
 */
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
  <main>
    <div class="article__back_head">
      <a href="<?php echo esc_url(home_url('blog/')); ?>">Back to blog</a>
    </div>
    <div class="article__head">
      <time><?php the_time('F j, Y'); ?></time>
      <h1><?php the_title(); ?></h1>
      <?php the_excerpt(); ?>
      <?php the_post_thumbnail('large'); ?>
    </div>
    <article class="article__details">
      <?php the_content(); ?>
    </article>
    <div class="article__back">
      <a href="<?php echo esc_url(home_url('blog/')); ?>" class="btn btn-stroke">Back</a>
    </div>
  </main>
<?php endwhile; ?>

<?php get_footer(); ?>
