<?php
/**
 * The default page template file
 */
?>

<?php get_header(); ?>

<main>
  <?php while (have_rows('components')) :
    the_row();
    $section_name = str_replace('_', '-', get_row_layout()); ?>

    <?php get_template_part("views/components/home/{$section_name}"); ?>

  <?php endwhile; ?>
</main>

<?php get_footer(); ?>
