<?php
/**
 * The template for displaying the footer
 */
?>

  <?php get_template_part('views/components/footer'); ?>

  <?php wp_footer(); ?>

</body>
</html>
