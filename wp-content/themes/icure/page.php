<?php
/**
 * The default page template file
 */
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
  <main>
    <div class="article__head">
      <h1><?php the_title(); ?></h1>
    </div>
    <article class="article__details">
      <?php the_content(); ?>
    </article>
  </main>
<?php endwhile; ?>

<?php get_footer(); ?>
